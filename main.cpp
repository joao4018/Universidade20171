#include <iostream>
#include "pessoa.hpp"
#include "aluno.hpp"

#include "professor.hpp"

using namespace std;

int main(int argc, char ** argv) {

   int i;
   // Criação de objetos com alocação estática
   Pessoa pessoa_1;
   Pessoa pessoa_2("Maria", "555-1111", 21);


   Professor * professor_1;
   professor_1 = new Professor();


	 pessoa_1.setNome("Joao");
	 pessoa_1.setMatricula("14/0078070");
	 pessoa_1.setTelefone("555-5555");
	 pessoa_1.setSexo("M");
	 pessoa_1.setIdade(20);

   professor_1->setNome("Ronal");
   professor_1->setMatricula("102/0152");
   professor_1->setTelefone("66983-6623");
   professor_1->setSexo("M");
   professor_1->setFormacao("Engenharia de Software");
   professor_1->setSalario(156);
   professor_1->setSala(15);






   Aluno *lista_de_alunos[20]; // Criação da lista de tamanho fixo de ponteiros do objeto Aluno

	 string nome;
	 string matricula;
	 int idade;
	 string sexo;
	 string telefone;
	 float ira;
	 int semestre;
	 string curso;


for(i=0;i<20;i++){
   // Entrada de dados do terminal (stdin)
   cout << "Nome: ";
   cin >> nome;
   cout << "Matricula: ";
   cin >> matricula;
   cout << "Idade: ";
   cin >> idade;
	 cout << "Sexo: ";
	 cin >> sexo;
	 cout << "Telefone: ";
	 cin >> telefone;
	 cout << "Ira: ";
	 cin >> ira;
	 cout << "Semestre: ";
	 cin >> semestre;
	 cout << "Curso: ";
	 cin >> curso;

   lista_de_alunos[i] = new Aluno();
	lista_de_alunos[i]->setNome(nome);
		lista_de_alunos[i]->setMatricula(matricula);
		lista_de_alunos[i]->setIdade(idade);
	 lista_de_alunos[i]->setSexo(sexo);
	 lista_de_alunos[i]->setTelefone(telefone);
	 lista_de_alunos[i]->setIra(ira);
	 lista_de_alunos[i]->setSemestre(semestre);
	 lista_de_alunos[i]->setCurso(curso);


	}
   // Impressão dos atributos do objeto
for(i=0;i<20;i++){
 lista_de_alunos[i]->imprimeDadosAluno();
}
  pessoa_1.imprimeDados();
  professor_1->imprimeDadosProfessor();
   // Liberação de memória dos objetos criados dinamicamente
   delete(professor_1);
	 delete(lista_de_alunos[20]);

   return 0;
}
